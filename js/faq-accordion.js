(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.faqAccordion = {
    attach: function (context, settings) {
      // Using once() to apply the myCustomBehaviour effect when you want to do just run one function.
      $(context).find('.faq-accordion-wrapper').once('myCustomBehavior').each ( function() {
        $(this).accordion({
          header: "h2.faq-accordion-header",
          heightStyle: "content"
        });
      });
    }
  };

})(jQuery, Drupal);